package com.michal.polach.morobaseapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class MoroBaseAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoroBaseAppApplication.class, args);
	}
}
