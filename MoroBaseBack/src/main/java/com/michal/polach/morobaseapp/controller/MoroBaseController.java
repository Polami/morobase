package com.michal.polach.morobaseapp.controller;

import com.michal.polach.morobaseapp.exception.ResourceNotFoundException;
import com.michal.polach.morobaseapp.model.User;
import com.michal.polach.morobaseapp.repository.UserRepository;
import com.michal.polach.morobaseapp.security.CustomUserDetailsService;
import com.michal.polach.morobaseapp.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class MoroBaseController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CustomUserDetailsService userDetailsService;

    @GetMapping("/users")
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public User getUserById(@PathVariable(value = "id") Long userId){
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", userId)
        );
    }

    @PostMapping("/users")
    public User addUser(@Valid @RequestBody User user){
        return userRepository.save(user);
    }

    @PutMapping("/users/{id}")
    public User updateUser(@PathVariable(value = "id") Long userId,
                           @Valid @RequestBody User userDetails){
        logoutIfPrincipalEdited(userId);

        User user = getUserById(userId);
        user.copyFrom(userDetails);
        return userRepository.save(user);
    }

    @DeleteMapping("/secured/deleteUser/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long userId){
        logoutIfPrincipalEdited(userId);

        User user = getUserById(userId);
        userRepository.delete(user);
        return ResponseEntity.ok().build();
    }

    private void logoutIfPrincipalEdited(Long editedUserId){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) return;
        if (auth.getPrincipal() instanceof UserPrincipal)
        {
            UserPrincipal principal = (UserPrincipal)auth.getPrincipal();
            if (principal.getId().equals(editedUserId))
            {
                auth.setAuthenticated(false);
            }
        }
    }
}
