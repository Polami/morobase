import React from "react";
import UserRow from "./UsersTableRow";
import UsersTableHeader from "./UsersTableHeader";
import PropTypes from "prop-types";

const UsersTableBody = (props) => {
    let rows = props.users.map((u) =>
        <UserRow key={u.id} user={u}
                 onRowDelete={props.onRowDelete}
                 onRowEdited={props.onRowEdited}/>
    )
    return (
        <tbody>
            <UsersTableHeader />
            {rows}
        </tbody>
    );
}

UsersTableBody.propTypes = {
    users: PropTypes.array,
    onRowDelete: PropTypes.func,
    onRowEdited: PropTypes.func
}

export default UsersTableBody;