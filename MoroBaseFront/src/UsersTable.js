import PropTypes from "prop-types";
import React from "react";
import UsersTableBody from "./UsersTableBody";

const UsersTable = (props) => {
    return (
        <div>
            <table className='Users'>
                <UsersTableBody users={props.users}
                                onRowDelete={props.onRowDelete}
                                onRowEdited={props.onRowEdited}/>
            </table>
        </div>
    );
}

UsersTable.propTypes = {
    users: PropTypes.array,
    onRowDelete: PropTypes.func,
    onRowEdited: PropTypes.func
}

export default UsersTable;