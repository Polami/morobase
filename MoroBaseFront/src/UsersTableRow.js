import React, {Component} from "react";
import UsersTableCell from "./UsersTableCell";
import PropTypes from "prop-types"
import EditButton from "./EditButtons";

class UsersTableRow extends Component{
    constructor(props){
        super(props);
        this.state = {
            editable: false,
            tempUser: Object.assign({}, this.props.user)
        };
    }

    handleEditRow(){
        this.setState({
            editable: true,
            tempUser: Object.assign({}, this.props.user)
        });
    }

    handleSaveRow(){
        this.setState({
            editable: false
        });
        this.props.onRowEdited(this.state.tempUser, this.update.bind(this));
    }

    // updates tempUser after receiving response from DB.
    // It refused to update automatically when changing users state of App.
    update(){
        this.setState({
            tempUser: Object.assign({}, this.props.user)
        });
    }

    handleCancelEdit(){
        this.setState({
            editable: false,
            tempUser: Object.assign({}, this.props.user)
        });
    }

    handleCellEdit(e, parse){
        let tUser = this.state.tempUser;
        let val = e.target.value;
        if (parse) val = parseInt(val, 10);
        tUser[e.target.name] = val;
        this.setState({
            tempUser: tUser
        });
    }

    render(){
        return (
            <tr>
                <UsersTableCell content={this.state.tempUser.id} editable={false}/>
                <UsersTableCell content={this.state.tempUser.name} editable={this.state.editable}
                                onEdit={this.handleCellEdit.bind(this)}
                                name="name"/>
                <UsersTableCell content={this.state.tempUser.username} editable={this.state.editable}
                                onEdit={this.handleCellEdit.bind(this)}
                                name="username"/>
                <UsersTableCell content={
                    <div>
                        <button onClick={e => this.props.onRowDelete(this.props.user.id, e)}>Smazat</button>
                        <EditButton onEdit={this.handleEditRow.bind(this)}
                                    onSave={this.handleSaveRow.bind(this)}
                                    onCancel={this.handleCancelEdit.bind(this)}
                                    editing={this.state.editable}/>
                    </div>
                }/>
            </tr>
        );
    }
}

UsersTableRow.propTypes = {
    user: PropTypes.object,
    onRowDelete: PropTypes.func,
    onRowEdited: PropTypes.func
};

export default UsersTableRow;