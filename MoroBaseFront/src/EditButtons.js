import React, {Component} from "react";
import PropTypes from 'prop-types';

class EditButtons extends Component{
    render() {
        if (this.props.editing) {
            return (
                <span>
                    <button onClick={this.props.onSave.bind(this)}>Ulozit</button>
                    <button onClick={this.props.onCancel.bind(this)}>Zrusit</button>
                </span>
            );
        }
        else {
            return (
                <button onClick={this.props.onEdit.bind(this)}>Upravit</button>
            );
        }
    }
}

EditButtons.propTypes = {
    editing: PropTypes.bool,
    onEdit: PropTypes.func,
    onSave: PropTypes.func,
    onCancel: PropTypes.func
};

export default EditButtons;