import React from "react";
import PropTypes from 'prop-types';

const UsersTableCell = (props) => {
    if (props.editable){
        return (
            <td>
                <input type={props.fieldType || "text"} value={props.content}
                       name={props.name}
                       onChange={props.onEdit}/>
            </td>
        );
    }
    return (
        <td>{props.content}</td>
    );
}

UsersTableCell.propTypes = {
    name: PropTypes.string,
    content: PropTypes.any,
    editable: PropTypes.bool,
    onEdit: PropTypes.func,
    fieldType: PropTypes.string
}

export default UsersTableCell;