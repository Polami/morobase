import React, {Component} from 'react';
import './App.css';
import PropTypes from 'prop-types';
import UsersTable from "./UsersTable";
import NewUser from  "./NewUser";
import request from "superagent"

const serverAddress ='http://localhost:8080';


class App extends Component {
    constructor(props){
        super(props);
        this.state = {users: []};
        this.deleteRow = this.deleteRow.bind(this);
        this.addRow = this.addRow.bind(this);
        this.editRow = this.editRow.bind(this);
        this.editRowSuccess = this.editRowSuccess.bind(this);
    }

    render() {
        return (
            <div>
                <UsersTable users={this.state.users} onRowDelete={this.deleteRow}
                            onRowEdited={this.editRow}/>
                <NewUser onRowAdded={this.addRow}/>
            </div>
        );
    }

    componentDidMount(){
        this.refreshTable();
    }

    refreshTable(){
        request.get(serverAddress + '/users')
            .then((res) =>{
                let usrs = res.body;
                this.setState({
                    users: usrs
                });
            })
            .catch((err) => {
                console.log(err.message);
            });
    }

    deleteRow(id){
        request.delete(serverAddress + '/secured/deleteUser/' + id)
            .withCredentials()
            .then((res) => {
                let updatedUsers = this.state.users;
                updatedUsers = updatedUsers.filter(user => user.id !== id);
                this.setState({
                    users: updatedUsers
                });
            })
            .catch((err) => {
                console.log(err.message);
            });
    }

    editRow(editedUser, callback){
        request.put(serverAddress + '/users/' + editedUser.id)
            .send(editedUser)
            .then((res) => this.editRowSuccess(res, editedUser))
            .catch((err) => {
                console.log(err.message);
            })
            .finally(callback);
    }

    editRowSuccess(res, editedUser){
        let index = this.state.users.map(u => u.id).indexOf(editedUser.id);
        let updatedUsers = this.state.users;
        updatedUsers[index] = editedUser;
        this.setState({
            users: updatedUsers
        });
    }

    addRow(name, username, password){
        request.post(serverAddress + '/users')
            .send({
                name: name,
                username: username,
                password: password
            })
            .then((res) => {
                let updatedUsers = this.state.users;
                updatedUsers.push(res.body);
                this.setState({
                    users: updatedUsers
                });
            })
            .catch((err) => {
                console.log(err.message);
            });
    }
}

App.propTypes = {
    users: PropTypes.array
}

export default App;
