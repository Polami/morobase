import React, {Component} from 'react';
import PropTypes from "prop-types"

class TextField extends Component {
    render() {
        return (
            <input value={this.props.text}
                   name={this.props.name}
                   placeholder={this.props.placeholder}
                   type={this.props.inputType || "text"}
                   onChange={this.props.onTextChange.bind(this)}/>
        );
    }
}

TextField.propTypes = {
    text: PropTypes.string,
    name: PropTypes.string,
    inputType: PropTypes.string,
    placeholder: PropTypes.string,
    onTextChange: PropTypes.func
}

export default TextField;