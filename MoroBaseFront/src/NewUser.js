import React, {Component} from "react";
import TextField from "./TextField";
import PropTypes from 'prop-types';

class NewUser extends Component{
    constructor(props){
        super(props);
        this.state = {
            name: "",
            username: "",
            password: ""
        }
    }

    handleCellChange(e){
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleAddClick(e){
        this.props.onRowAdded(this.state.name, this.state.username, this.state.password);
        this.setState({
            name: "",
            username: "",
            password: ""
        });
    }

    render() {
        return (
            <fieldset className="NewUser">
                <legend>Add user</legend>
                    <TextField text={this.state.name}
                               placeholder="Name"
                               name="name"
                               onTextChange={this.handleCellChange.bind(this)}/>
                    <TextField text={this.state.username}
                               placeholder="Username"
                               name="username"
                               onTextChange={this.handleCellChange.bind(this)}/>
                    <TextField text={this.state.password}
                               placeholder="Password"
                               inputType="password"
                               name="password"
                               onTextChange={this.handleCellChange.bind(this)}/>
                    <button onClick={this.handleAddClick.bind(this)}>
                        Add
                    </button>
            </fieldset>
        );
    }
}

NewUser.propTypes = {
    onRowAdded: PropTypes.func
}

export default NewUser;