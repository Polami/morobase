import React from "react";

const UsersHeader = (props) => {
    return (
        <tr className='Header'>
            <th>ID</th>
            <th>Name</th>
            <th>Username</th>
            <th>Edit</th>
        </tr>
    );
}

export default UsersHeader;